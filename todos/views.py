import imp
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy
# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "item/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "item/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")